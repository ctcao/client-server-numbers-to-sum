package com.lilun;

/*
 * Objective:
 * To make sure our application is accessed by humans and not a by any computers or bot.
 *
 * Description:
 * We want to make sure that the users who are using our application knows how to add numbers. To 
 * achieve this, we want to provide the user/client with a question and then allow the user/client 
 * to submit an answer. If the user/client and the service were both real person, then they would 
 * have interacted in the following manner:

    Client: Hey Service, can you provide me a question with numbers to add?
    Service: Here you go, solve the question: “Please sum the numbers 9,5,3”.
    Client: Great. The original question was “Please sum the numbers 9,5,3” and the answer is 15.
    Service: That’s wrong. Please try again.
    Client: Sorry, the original question was “Please sum the numbers 9,5,3” and the answer is 17.
    Service: That’s great
 * 
 * @author Lilun Cao
 */


import java.security.Principal;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;  

@RestController
public class SumRestController {	
	@RequestMapping(value="/", method=RequestMethod.POST, headers="Accept=application/json") 
	public ResponseEntity services(@RequestBody RequestMessage reqMsg, HttpServletRequest request) {
		try {
			String msg = reqMsg.getMessage();
			int serviceId = reqMsg.getServiceId();
			System.out.println("Message: " + msg);

			if (serviceId == 2) {// verify the answer
				try {
					boolean isSumCorrect = new ServiceUtils().validateAnswer(msg);
					String respMsg = "";
					if (isSumCorrect) {
						respMsg = "That's great";
						return new ResponseEntity<String>(respMsg,HttpStatus.OK);
					} else {
						respMsg = "That's wrong. Please try again.";
						return new ResponseEntity<String>(respMsg,HttpStatus.BAD_REQUEST);
					}
				}catch(Exception ex) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
				}
			} else if (serviceId == 1) { // question
				String resp =  new ServiceUtils().getQuestion();

				System.out.println(resp);
				return new ResponseEntity<String>(resp,HttpStatus.OK);
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
			}
		} catch(Exception ex) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
}
