package com.lilun;

import org.springframework.boot.SpringApplication;  
import org.springframework.boot.autoconfigure.SpringBootApplication;  

@SpringBootApplication  
public class SpringBootSmartEquipSumApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootSmartEquipSumApplication.class, args);
	}
}
