package com.lilun;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SumRestControllerTest {
	   @LocalServerPort
	    private int port;

	    private URL base;

	    @Autowired
	    private TestRestTemplate template;

	    @Before
	    public void setUp() throws Exception {
	    	//port = 8080;
	        this.base = new URL("http://localhost:" + port + "/");
	    }

	    @Test
	    public void getQuestion() throws Exception {
	    	RequestMessage reqMsg = new RequestMessage();
	    	reqMsg.setMessage("Hey Service, can you provide me a question with numbers to add?");
	    	reqMsg.setServiceId(1);
	    	
	        ResponseEntity<String> response = template.postForEntity(base.toString(),reqMsg,
	                String.class);
	        System.out.println(response.getBody());
	        assertThat(response.getBody(), containsString("solve the question"));
	    }
	    
	    @Test
	    public void getIncorrectAnswer() throws Exception {
	    	RequestMessage reqMsg = new RequestMessage();
	    	reqMsg.setMessage("Great. The original question was \"Please sum the numbers 9,5,3\" and the answer is 15.");
	    	reqMsg.setServiceId(2);
	    	
	        ResponseEntity<String> response = template.postForEntity(base.toString(),reqMsg,
	                String.class);
	        System.out.println(response.getBody());
	        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
	        assertThat(response.getBody(), containsString("That's wrong. Please try again"));
	    }	
	    
	    @Test
	    public void getCorrectAnswer() throws Exception {
	    	RequestMessage reqMsg = new RequestMessage();
	    	reqMsg.setMessage("Great. The original question was \"Please sum the numbers 9,5,3\" and the answer is 17.");
	    	reqMsg.setServiceId(2);
	    	
	        ResponseEntity<String> response = template.postForEntity(base.toString(),reqMsg,
	                String.class);
	        System.out.println(response.getBody());
	        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
	        assertThat(response.getBody(), containsString("That's great"));
	    }	
	    
	    @Test
	    public void testBadRequest() throws Exception {
	    	RequestMessage reqMsg = new RequestMessage();
	    	reqMsg.setMessage("Great. The original item was 17.");
	    	reqMsg.setServiceId(2);
	    	
	        ResponseEntity<String> response = template.postForEntity(base.toString(),reqMsg,
	                String.class);
	        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
	        assertThat(response.getStatusCode().BAD_REQUEST, equalTo(HttpStatus.BAD_REQUEST));
	    }
	    
	    @Test
	    public void testNonServices() throws Exception {
	    	RequestMessage reqMsg = new RequestMessage();
	    	reqMsg.setMessage("Great. The the result is 17.");
	    	reqMsg.setServiceId(3);
	    	
	        ResponseEntity<String> response = template.postForEntity(base.toString(),reqMsg,
	                String.class);
	        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
	        assertThat(response.getStatusCode().BAD_REQUEST, equalTo(HttpStatus.BAD_REQUEST));
	    }	    
}
