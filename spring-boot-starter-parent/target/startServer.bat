REM The default port number is 8080 but a different port number could be used by using
REM the option -Dserver.port=8080. If this option is not provided, the default port number
REM would be used.

java -Dserver.port=8080 -jar SumRestService-0.0.1-SNAPSHOT.jar

pause