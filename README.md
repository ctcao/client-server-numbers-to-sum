# Services to clients to sum the numbers

We want to make sure that the users who are using our application knows how to add numbers. To achieve this, we want to provide the user/client with a question and then allow the user/client to submit an answer. If the user/client and the service were both real person, then they would have interacted in the following manner:

```
Client: Hey Service, can you provide me a question with numbers to add?
Service: Here you go, solve the question: �Please sum the numbers 9,5,3�.
Client: Great. The original question was �Please sum the numbers 9,5,3� and the answer is 15.
Service: That�s wrong. Please try again.
Client: Sorry, the original question was �Please sum the numbers 9,5,3� and the answer is 17.
Service: That�s great
```

## Instruction

(1) The project was developed using using Java8 and Spring Boot with latest version of Eclipse on windows 10: 

Eclipse Java EE IDE for Web Developers.

Version: Oxygen.2 Release (4.7.2)
Build id: 20171218-0600

(2) The jdk version used to build the project is:

```
    jdk 1.8.0_161
```

(3) The services will accept requests in the format of json message like

```
{

   "serviceId": 1,	
   "message": "Hey Service, can you provide me a question with numbers to add?"

}
```

(4) The services will send the responses back in the format of string with http status code like

Here you go, solve the question: �Please sum the numbers 9,5,3�.

(5) The services have a unit test class called SumRestControllerTest.java under the package src/test/java/com/lilun. 
It can be run as JUnit Test independently in eclipse once the services are started at the port number 8080. Here for testing purpose,
the port number 8080 was hardcoded in the file SumRestControllerTest.java. This class tests different cases: asking the service to
provide numbers to sum, sending answer back to service to verify the correctness, and sending bad request, etc.
    
(6) The services are stateless.

(7) The services should serve a single path �/�.

## Installation

(0) Download and install jdk 1.8 on your server if you didn't do it.

(1) Download the packaged jar file SumRestService-0.0.1-SNAPSHOT.jar and the windows
batch file startServer.bat from my gitlab <https://gitlab.com/ctcao/client-server-numbers-to-sum/tree/master/spring-boot-starter-parent/target> to a local folder

(2) Change to that folder

(3) Double click the file startServer.bat to start the server

(4) The services would be available through the following URL:

```
    http://localhost:8080/
```

(5) The services use the default port number. It can be changed to other port number in the batch file startServer.bat.

## Test the services

(1) Install postman extension to chrome browser

(2) The services can be Run As Java Application in Eclipse or can be started by double clicking the downloaded 
batch file startServer.bat.

(3) Once the services are started, add the url "localhost:8080/" to postman and select method POST, 
past the following json message in the request body:

```
{
	
   "serviceId": 2,
   "message": "Great. The original question was \"Please sum the numbers 9,5,3,8\" and the answer is 15"

}
```

Then click Send button. The response body will be shown in the bottom window and http status code will be also 
shown.

That's wrong. Please try again.

(4) For other requests, just change the message.

## Assumptions and trade-offs

(1) The request is sent to the server by POST method in the json format.

(2) The response is sent back in the string format. It could be modified to use json message like request.

(3) The numbers to be added are randomly selected between 1 and 100.

(4) 2 to 5 numbers will be selected for each request for some numbers to add.

(5) If serviceId = 1, then this request will be assumed to be a request for asking some numbers to sum.

(6) If serviceId = 2, then this request will be assumed to be a request to give the answer to
ask the server to verify that.

(7) In real situation, we must use some methods to identify the types of requests.  

(8) For Safeguard against cheating, we can use a token for each user. Only authenticated users can use the services,so it is
possible for each user to have a unique token. The token will be used for each request. When the user asks the
services to  provide numbers to sum, the token with the question could be saved somewhere like in the database or
a services log file in json format where each user's requests were saved as a list. When the user sends back the response, the question will 
be checked against the old requests saved in the file
or the database to verify whether the question and answer come from the same user. If I have mor time, I will
implement this.


